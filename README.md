# NOMBRE DEL PROYECTO GI_vnavarro129

# DESCRIPCIÓN

Desarrollo de Trabajos Prácticos del Curso de Gestión de la Información de la Maestría en Tecnología Satelital.-

# ESTRUCTURA - CONTENIDO

El proyecto esta organizado en carpetas de la siguiente forma:


/FC/Informe: se encuentra un archivocon extensión .pdf en cual se explica el trabajo realizado en el proyecto

/FC/Frame : se encuentran el archivo que contiene la trama de TM con la extensión .bin 

/FC/TSL : se encuentran el archivo con extensión .tsl con los scripts que se generaron para cumplir con los requerimientos solicitados 

/FC/SCL :  se encuentran el archivo con extensión .scl con los scripts que se generaron para cumplir con los requerimientos solicitados

/FC/Viewer : se encuentran los archivos que se generaron para el View de la trama de TM utilizando el software de CONAE Tlmy View


## Trabajo Práctico Nº1: Lenguajes SCL y TCL. Práctica con Git

Para la realización de este Práctico se utilizaron los siguientes recursos de Software:
- [ ] 010 Editor
- [ ] Notepad++
- [ ] Software CONAE  [Tlmyview, SInter]





- Para el Caso de TSL realizar un View donde se muestren:


- [ ] El modo operativo en texto de la plataforma (" CIENCIA", "PROPULSION", "SURVIVAL" o "SAFE HOLD"). Usar posición 30 de la telemetría tomando los 2 bits menos significativos.

```
/-----------------Modo operativo --------------------------------
tsl Tlmy::MV.Mode.Raw
return 2bits : out
label
  -- Modo operativo 
description
  -- Modo Operativo:
  --00:CIENCIA
  --01:PROPULSION
  --10:SURVIVAL
  --11:SAFE HOLD
{
    out = ExtractTlmyBits(30, 0, 2);  // se extrae 2 bits  en la posicion  byte =30 
}

tsl Tlmy::MV.Mode.Txt
return string : out
label
  -- Modo operativo
  description
  {
	if (MV.Mode.Raw == <bin\ 00 >)
		out = "CIENCIA";
	else if (MV.Mode.Raw == <bin\ 01 >)
		out = "PROPULSION";
	else if (MV.Mode.Raw == <bin\ 10 >)
  	out = "SURVIVAL";
	else if (MV.Mode.Raw == <bin\ 11 >)
		out = "SAFE HOLD";
	}

```


- [ ] El TXS activo en texto (“TXS1”,”TXS2”,”ERROR”). Usar 1 byte de la posición 31. Usar 2 bits menos significativos.










- [ ] El OBT en formato time. Elegir la posición 32 a 35.
```
//--------------OBT  formato Time-----------------------------------
tsl Tlmy::SYS.TimeStamp
return  time : out
label
  --  OBT UTC
description
  --  On Board Time UTC format
local
  time : tmpDate;
  bytes : extracted;
  integer : extractedToInt;
{
    extracted = ExtractTlmyBytes(32, 4);
    extractedToInt = extracted;
	tmpDate = 1970/01/06 00:00:00 + extractedToInt;
    out = tmpDate;
}

```

- [ ] El tiempo del FC en formato time (sin resolución de millisegundos). CASTime.
- [ ]  Convertir a un rango de 0v a 10v un byte de la posición 40. Tomando 2 bytes. \item Generar alarmas en rojo para menos de 3v y más de 7v. En verde para el resto.
- [ ] El Contador de Frames.

Generar un archivo con al menos 10 tramas en las que se pueda verificar por el View los valores de cada variable.


- Para el Caso de SCL escribir un script que:

- [ ] Informe a un archivo de log cada una de las actividades que va realizando.
En el archivo de log deberá aparecer además del mensaje de log la hora en el que se generó el mismo.
- [ ] Pregunte al operador el nombre sesión. (Ej: pasada1234)
- [ ] Abra una sesión en las unidades del RTTMRecMng y CtrlCons.
- [ ] Configurar la SCCE para trabajar en Close Loop.
- [ ] Pregunte al operador para ejecutar el envío de comandos.
- [ ] Se envíen 100 comandos.
- [ ] Cierre la sesión de RTTMRecMng y CtrlCons.

- Práctica con Git 

- [ ] Subir las versiones de los scripts que se vayan creando de los SCLs y TSLs

- [ ] Separar en Carpetas con la estructura Siguiente:


```
/FC/SCL
/FC/TSL
/FC/Viewer

```
Para generar esta estructura de directorios se utilizó los siguientes comandos:





## Trabajo Práctico Nº2:  XCTE



## Trabajo Práctico Nº3: Payloads de Satélites de Observación de la Tierra
# ESTRUCTURA - CONTENIDO
Contenido de este directorio es un informe con extensión en .pdf.
```
/TPNº3 GI_Vnavarro_MTS.pdf

```

# AUTOR
VALERIA NAVARRO - MTS
